# What

A docker image to run the firestore emulator. 

# Why

I don't like installing tools locally, I need it to work on ci platforms, when I
started other images were not up to date. 

# versions

- 7.6.1
- 7.16.2, latest

