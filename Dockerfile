FROM node:lts-alpine

ARG FIREBASE_TOOLS_VERSION=7.16.2
RUN yarn global add firebase-tools@${FIREBASE_TOOLS_VERSION} && \
    yarn cache clean && \
    firebase -V && \
    mkdir $HOME/.cache

RUN apk --no-cache add openjdk8-jre 
RUN firebase setup:emulators:firestore

EXPOSE 8080

ENTRYPOINT ["firebase"]
CMD ["emulators:start", "--only", "firestore"]
